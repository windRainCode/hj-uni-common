/*
 * @Author: Zhang Wei
 * @Date: 2022-12-12 21:37:03
 * @LastEditors: Zhang Wei
 * @LastEditTime: 2022-12-12 21:41:54
 * @FilePath: /hj-uni-common/test.js
 * @Word: There is nothing either good or bad, but thinking makes it so.
 * @Description:
 */

// 基础相关
export * from './src/base/index.js';

// 业务相关
export * from './src/business/index.js';
