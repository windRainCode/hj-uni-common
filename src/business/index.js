/*
 * @Author: Zhang Wei
 * @Date: 2022-12-12 21:13:11
 * @LastEditors: Zhang Wei
 * @LastEditTime: 2022-12-17 15:46:37
 * @FilePath: /hj-uni-common/src/business/index.js
 * @Word: There is nothing either good or bad, but thinking makes it so.
 * @Description: 业务相关模块
 */

import { StringUtils } from '../base/index.js';

// imei工具类
export const ImeiUtils = {
  analysis: function (qrCodeContent) {
    if (qrCodeContent == null || qrCodeContent == '' || qrCodeContent == undefined) {
      return '';
    }
    let imei = '';
    let isImeiQr = qrCodeContent.indexOf('deviceNo=');
    let isTermQr = qrCodeContent.indexOf('terminalNo=');
    let islSGC = qrCodeContent.indexOf('IMEI-');
    if (isImeiQr <= 0 && isTermQr <= 0 && islSGC <= 0) {
      if (qrCodeContent.indexOf('web-wechart/s/') != -1) {
        let cd = qrCodeContent.replace(/.*\//, '');
        imei = cd;
      } else {
        imei = ImeiUtils.analysisImei(qrCodeContent);
      }
    } else {
      if (islSGC > 0) {
        imei = ImeiUtils.analysisLSGC(qrCodeContent);
      } else {
        imei = ImeiUtils.analysisUrl(qrCodeContent);
      }
    }
    if (StringUtils.isBlank(imei)) {
      return qrCodeContent;
    }
    return imei;
  },

  isTermQrCode: function (qrCodeContent) {
    let isTermQr = qrCodeContent.indexOf('terminalNo=');
    return isTermQr > 0;
  },

  //解析模块二维码
  analysisImei: function (qrCodeContent) {
    let pref = 'IMEI:';
    let values = qrCodeContent.split(';');
    if (values.length == 1) {
      values = qrCodeContent.split(',');
    }
    if (values.length > 1) {
      for (let i = 0; i < values.length; i++) {
        let info = values[i];
        let preIndex = info.indexOf(pref);
        if (preIndex > -1) {
          let imei = info.slice(preIndex + pref.length, info.length); //解析扫码值
          return imei;
        }
      }
    }
    return;
  },

  //解析URL二维码
  analysisUrl: function (qrCodeContent) {
    let length = qrCodeContent.indexOf('&');
    if (length == null || length <= 0 || length == undefined || length == NaN) {
      length = qrCodeContent.length;
    } else {
      length = length + 1;
    }
    let equipCd = qrCodeContent.substring(qrCodeContent.indexOf('=') + 1, length);
    return equipCd;
  },

  //解析龙尚国产的IMEI
  analysisLSGC: function (value) {
    //龙尚国产芯片
    let length = value.length;
    let preIndex = value.indexOf('IMEI-');
    if (preIndex < 0) {
      return value;
    }
    let imei = value.substring(preIndex + 5, length);
    return imei;
  },
};

/**
 * GPS工具类
 */
export const GpsUtils = {
  // 将高德（微信）坐标转换为百度坐标
  translateToBaidu: function (lng, lat) {
    let x_PI = (Math.PI * 3000.0) / 180.0;
    let z = Math.sqrt(lng * lng + lat * lat) + 0.00002 * Math.sin(lat * x_PI);
    let theta = Math.atan2(lat, lng) + 0.000003 * Math.cos(lng * x_PI);
    let bd_lng = z * Math.cos(theta) + 0.0065;
    let bd_lat = z * Math.sin(theta) + 0.006;
    return { lng: bd_lng, lat: bd_lat };
  },

  // 将百度坐标转换为高德(微信)坐标
  translateToGaode: function (lng, lat) {
    let X_PI = (Math.PI * 3000.0) / 180.0;
    let x = lng - 0.0065;
    let y = lat - 0.006;
    let z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * X_PI);
    let theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * X_PI);
    let gg_lng = z * Math.cos(theta);
    let gg_lat = z * Math.sin(theta);
    return { lng: gg_lng, lat: gg_lat };
  },
};
