/*
 * @Author: Zhang Wei
 * @Date: 2022-12-12 21:12:20
 * @LastEditors: Zhang Wei
 * @LastEditTime: 2022-12-12 21:16:26
 * @FilePath: /hj-uni-common/src/base/index.js
 * @Word: There is nothing either good or bad, but thinking makes it so.
 * @Description: 基础模块
 */

/**
 * 字符串操作
 */
export const StringUtils = {
  // 判断字符串是否有内容, 如果为null,undefined,空,多个空格都返回true
  isBlank: function (str) {
    if (!str) {
      return true;
    } else {
      str = str.trim();
      return str.length == '0';
    }
  },

  // 判断字符串是否已给定字符开头, 如: StringUtils.isStartWith('asdf','as') --> true
  isStartWith: function (str, start) {
    if (StringUtils.isBlank(str) || StringUtils.isBlank(start)) {
      return false;
    }
    return str.indexOf(start) == 0;
  },

  // 拼接字符串, 按拼接顺序一次传入, 如: StringUtils.join(2,3,'aaa') --> '23aaa'
  join: function () {
    if (arguments && arguments.length > 0) {
      let sBuffer = new StringBuffer();
      for (let i = 0; i < arguments.length; i++) {
        sBuffer.append(arguments[i]);
      }
      return sBuffer.toString();
    }
    return COMMON.blank;
  },
};
