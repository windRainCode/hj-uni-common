/*
 * @Author: Zhang Wei
 * @Date: 2022-12-14 20:11:38
 * @LastEditors: Zhang Wei
 * @LastEditTime: 2022-12-14 20:16:32
 * @FilePath: /hj-uni-common/src/H5Common/wechart/openLocation.js
 * @Word: There is nothing either good or bad, but thinking makes it so.
 * @Description:
 */

// 打开位置
export function openLocation(options, data, wx) {
  options = getNewOptions(options);

  // 获取授权
  wx.config({
    debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
    appId: data.appid, // 必填，公众号的唯一标识
    timestamp: data.timestamp, // 必填，生成签名的时间戳
    nonceStr: data.nonceStr, // 必填，生成签名的随机串
    signature: data.sign, // 必填，签名，见附录1
    jsApiList: ['openLocation', 'checkJsApi'], // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
  });

  wx.error(function (res) {
    // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
    console.log('res:', res);
    // uni.$msg(JSON.stringify(res));
    if (typeof options.wxErrCb === 'function') {
      options.wxErrCb(res);
    }
  });

  wx.ready(function () {
    wx.openLocation({
      ...options,
    });
  });
}

// 设置默认 选项
function getNewOptions(mineOptions = {}) {
  let defaultOptions = {
    // latitude: latitude, // 纬度，浮点数，范围为90
    // longitude: longitude, // 经度，浮点数，范围为180
    // name: name, // 位置名
    // address: address, // 地址详情说明
    scale: 18, // 地图缩放级别,整形值,范围从1~28。默认为最大
    infoUrl: '', // 在查看位置界面底部显示的超链接,可点击跳转
    success: function (res) {},
    fail: function (res) {},
    cancel: function (res) {},
    complete: function (res) {},
    // 微信报错处理
    wxErrCb: function (res) {},
  };
  return Object.assign(defaultOptions, mineOptions);
}
