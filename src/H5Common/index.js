/*
 * @Author: Zhang Wei
 * @Date: 2022-08-08 20:20:12
 * @LastEditors: Zhang Wei
 * @LastEditTime: 2022-12-14 19:22:07
 * @FilePath: /hj-uni-common/src/H5Common/index.js
 * @Word: There is nothing either good or bad, but thinking makes it so.
 * @Description:
 */
/*===============  端模块  ===============*/
import { WxObj } from './wechart/index.js';

// 默认参数
let defaultOptions = {
  // 系统参数
  sysAppid: null,

  // 微信请求接口
  jsApiWxpay: null,

  wx: {},

  // 全局uni对象
  // uni: {},
};

export class commonObj {
  targetObj = null;
  targetShellName = null;

  // 公共uni对象 （主要从外部引入全局 uni）
  uni = {};

  constructor(options = defaultOptions) {
    let { shellName } = this.getBrowser();
    this.targetShellName = shellName;

    let { sysAppid, jsApiWxpay, wx } = options;

    switch (shellName) {
      case 'wxpay':
        this.targetObj = WxObj.init(sysAppid, jsApiWxpay, wx);
        break;

      default:
        // uni.$msg('环境判断失败,请在微信或支付宝中打开');
        this.targetObj = WxObj.init(sysAppid, jsApiWxpay, wx);
        break;
    }

    if (this.targetObj) {
      this.targetObj.$parent = this;
      this.targetObj.getBrowser = this.getBrowser;
    }

    // 获取uni引用 修改uni方法
    this.uni = options.uni || {};
  }

  // 获取当前浏览器状态
  getBrowser() {
    // 参考：http://www.manongjc.com/detail/28-wlzwkhgrsrprtnn.html
    // 如果要更高级，可以考虑：http://www.manongjc.com/detail/28-wlzwkhgrsrprtnn.html
    const { userAgent, browserLanguage, language } = navigator;

    const outputDevice = {
      isIos: !!userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), // ios 终端
      isMobile: !!userAgent.match(/AppleWebKit.*Mobile.*/), // 是否移动终端
      isAndroid: userAgent.indexOf('Android') > -1 || userAgent.indexOf('Linux') > -1, // android 终端端或者uc 浏览器
      isIphone: !!userAgent.indexOf('iPhone') > -1, // 是否为iPhone或 QQHD浏览器
      iPad: userAgent.indexOf('iPad') > -1, // 是否是iPad
      isWebApp: userAgent.indexOf('Safari') === -1, // 是否为Webapp，没有头部和底部
      isTrident: userAgent.indexOf('Trident') > -1, //IE内核
      isPresto: userAgent.indexOf('Presto') > -1, // opera 内核
      isWebKit: userAgent.indexOf('AppleWebkit') > -1, // 苹果谷歌内核
      isGecko: userAgent.indexOf('Gecko') > -1 && userAgent.indexOf('KHTML') === -1, //火狐内核
      isWechat: !!userAgent.match(/micromessenger/gi),
      isWeiBo: !!userAgent.match(/weibo/gi),
      isAlipay: userAgent.match(/weibo/gi), // 支付宝
      isQQ: !!userAgent.match(/qq/gi),
    };

    const outputLang = (browserLanguage || language).toLowerCase();

    let shellName = null;
    if (outputDevice.isWechat) {
      shellName = 'wxpay';
    } else if (outputDevice.isAlipay) {
      shellName = 'alipay';
    }

    this.shellName = shellName;

    return {
      outputDevice,
      outputLang,
      shellName,
    };
  }

  // 覆盖默认 uni 属性
  install(uni, tObj) {
    // let tObj = this.targetObj;
    // ===== 这种方法在本地可以用，但是在测试就不能用 ===== //
    // 改造uni
    // if (tObj) {
    //   // 遍历所有方法
    //   let nameList = Object.getOwnPropertyNames(Object.getPrototypeOf(tObj)) || [];
    //   console.log(nameList, 'nameList');
    //   nameList.forEach((name) => {
    //     let item = tObj[name];
    //     if (name === 'constructor') return false;
    //     if (typeof item === 'function') {
    //       // 劫持uni.原有方法
    //       if (uni[name]) uni['$' + name] = uni[name];
    //       uni[name] = (...arg) => {
    //         return tObj[name](...arg);
    //       };
    //     }
    //   });
    // }
    // 同理
    // if (tObj.scanCode) {
    //   // uni.$scanCode = uni.scanCode;
    //   uni.scanCode = (...arg) => {
    //     tObj.scanCode(...arg);
    //   };
    // }
    // if (tObj.pay) {
    //   // uni.$pay = uni.pay;
    //   uni.pay = (...arg) => {
    //     tObj.pay(this.targetObj, ...arg);
    //   };
    // }
  }
}
