/*
 * @Author: Zhang Wei
 * @Date: 2022-12-12 20:13:16
 * @LastEditors: Zhang Wei
 * @LastEditTime: 2022-12-14 14:28:43
 * @FilePath: /hj-uni-common/src/index.js
 * @Word: There is nothing either good or bad, but thinking makes it so.
 * @Description: 后稷公共服务
 */
// 基础相关
export * from './base/index.js';

// 业务相关
export * from './business/index.js';

// H5 相关
export * from './H5Common/index.js';

// 是否需要H5
// export const getH5Obj = async () => {
//   const { commonObj } = await import('./H5Common/index.js');
//   return commonObj;
// };

/*===============  测试代码 start  ===============*/
// import { ImeiUtils } from './test.js';
// console.log(ImeiUtils);
/*===============  测试代码 end  ===============*/
