# uni-app 公共工具类



## 安装命令

~~~sh
npm install hj-uni-common --save
~~~



## commonObj（一般在uni.$H5）

> 适配H5 微信jssdk 和 支付宝

::: details 文件引入示例

> main.js 中

~~~javascript
// #ifdef H5
import { installH5 } from '@/utils/h5';
// #endif

// #ifdef H5
// 微信、支付宝 jssdk
let { $H5 } = installH5(hj.commonObj);
uni.$H5 = $H5;
// #endif
~~~

> /src/utils/h5.js

~~~javascript
import wx from 'weixin-js-sdk';

// 校验接口
import { jsApiWxpay } from '@/service/common';

// 重新组装api
const installH5Api = (tObj) => {
  //  扫码
  uni.scanCode = (...args) => {
    return tObj.scanCode(...args);
  };

  // 支付
  uni.pay = (...args) => {
    return tObj.pay(...args);
  };
};

// 引入H5文件
export const installH5 = (commonObj) => {
  let h5options = {
    jsApiWxpay,
    uni: uni,
    sysAppid: '20220322391mFrgoH37',
    wx,
  };
  let $objH5 = new commonObj(h5options);
  let $H5 = null;
  // 劫持uni原有方法
  if ($objH5.targetObj) {
    $H5 = $objH5.targetObj;
    installH5Api($H5);
  }

  return { $H5 };
};
~~~

:::



### uni.$H5.getBrowser

> 获取浏览器信息

返回对象

~~~javascript
{
	outputDevice, // 设备信息
	outputLang, // 语言
	shellName // 端名称（wxpay | alipay）
}
~~~



### uni.scanCode

> 二维码扫码

~~~javascript
uni.scanCode({
  scanType: ['qrCode'],
  success(res) {
    let result = ImeiUtils.analysis(res.result)
    formData.value.cd = result
  },
})
~~~



### uni.pay

> 支付，这里的支付参数（payData.value）需要向后台请求

~~~javascript
uni.$H5.pay(payData.value).then((res) => {
	console.log(res, 'res')
})
~~~






## StringUtils

> 字符串操作



### isBlank

> 判断字符串是否有内容, 如果为null,undefined,空,多个空格都返回true



### isStartWith

> 判断字符串是否已给定字符开头, 如: StringUtils.isStartWith('asdf','as') --> true



### join

> 拼接字符串, 按拼接顺序一次传入, 如: StringUtils.join(2,3,'aaa') --> '23aaa'



## ImeiUtils

> imei工具类



### analysis

> 解析Imei字符（一般用于二维码扫描）



### isTermQrCode

> 判断是否为 `terminalNo`



### analysisImei

> 解析模块二维码



### analysisUrl

> 解析URL二维码



### analysisLSGC

> 解析龙尚国产的IMEI



## GpsUtils

> 位置信息处理工具



### translateToBaidu

> 将高德（微信）坐标转换为百度坐标

~~~javascript
function (lng, lat)
~~~



### translateToGaode

> 将百度坐标转换为高德(微信)坐标

~~~javascript
function (lng, lat)
~~~

# 升级日志

2022-12-17 16:47:24 新增 GpsUtils 工具类 [v0.0.18]